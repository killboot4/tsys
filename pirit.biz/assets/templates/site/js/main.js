function sidebar_stick (window_w){
	if (!window_w){ 
		window_w = $(window).width();
	}else if(window_w){
		window_w = window_w.width();
	}
	var sidebar = $('.sidebar');
	if (window_w <= 991) {
		sidebar.trigger("sticky_kit:detach");;
	}else{
		if(sidebar.length){
			sidebar.stick_in_parent();
		}
	}
}

$(document).ready(function(){
	$('.slider_related').owlCarousel({
	    loop:true,
	    nav:true,
	    dots:true,
	    margin:10,
		navContainer: '.owl-navs--slider',
		dotsContainer: '.owl-dots--slider',
		responsive:{
			0:{
				items:1,
				slideBy:1
			},
			485:{
				items:2,
				slideBy:2
			},
			767:{
				items:3,
				slideBy:3
			},
			1025:{
				items:4,
				slideBy:4
			}
		}
	});
	$('.inside_one--articles').owlCarousel({
	    loop:true,
	    nav:false,
	    dots:true,
	    autoplay: true,
	    responsive:{
	    	0:{
				margin: 0,
				items: 1
			},
			768:{
				margin: 30,
				items: 2
			},
			992:{
				items: 1
			}
		}
	});
	$('.slider--partners').owlCarousel({
	    loop:true,
	    nav:true,
	    dots:false,
	    // autoplay: true
	    navText: [
		    '<svg width="14" height="24" viewBox="0 0 14 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.93934 10.9393C0.353553 11.5251 0.353553 12.4749 0.93934 13.0607L10.4853 22.6066C11.0711 23.1924 12.0208 23.1924 12.6066 22.6066C13.1924 22.0208 13.1924 21.0711 12.6066 20.4853L4.12132 12L12.6066 3.51472C13.1924 2.92893 13.1924 1.97918 12.6066 1.3934C12.0208 0.807612 11.0711 0.807612 10.4853 1.3934L0.93934 10.9393ZM3 10.5H2L2 13.5H3V10.5Z" fill="#909090"/></svg>',
		    '<svg width="14" height="24" viewBox="0 0 14 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.93934 10.9393C0.353553 11.5251 0.353553 12.4749 0.93934 13.0607L10.4853 22.6066C11.0711 23.1924 12.0208 23.1924 12.6066 22.6066C13.1924 22.0208 13.1924 21.0711 12.6066 20.4853L4.12132 12L12.6066 3.51472C13.1924 2.92893 13.1924 1.97918 12.6066 1.3934C12.0208 0.807612 11.0711 0.807612 10.4853 1.3934L0.93934 10.9393ZM3 10.5H2L2 13.5H3V10.5Z" fill="#909090"/></svg>'
		],
		responsive:{
			0:{
				margin: 0,
				items: 1
			},
			480:{
				margin: 20,
				items: 2
			},
			768:{
				items: 3
			},
			991:{
				margin: 30,
				items: 3
			},
			1100:{
				margin: 70,
				items: 5
			}
		}
	});
	$('.menu_sidebar li.dropdown_li .arrow_drop_open').on('click', function(e){
		e.preventDefault();
		var _this = $(this),
			dropdown_li = '.dropdown_li';
			li = _this.parents(dropdown_li);


		if (li.hasClass('open')) {
			li.removeClass('open');
		}else{
			$(dropdown_li).removeClass('open');
			li.addClass('open');
		}
	});
	sidebar_stick();
});

$(window).on('resize', function(){
	sidebar_stick($(this));
});