$(document).ready(function() {
    if ($("#form_register").length > 0) {
        var rules = {
            name: {
                required: !0
            },
            fio: {
                required: !0
            },
            email: {
                required: !0
            },
            phone: {
                required: !0
            },
            company: {
                required: !0
            }
        };
        $.validator.messages.required = null, $("#form_register").validate({
            wrapper: null,
            errorLabelContainer: null,
            errorContainer: null,
            focusInvalid: !1,
            focusCleanup: !0,
            submitHandler: function(form) {},
            errorPlacement: function() {},
            invalidHandler: function(event, validator) {
                validator.numberOfInvalids() && $("html, body").animate({
                    scrollTop: $(validator.errorList[0].element).offset().top - 250
                }, 1e3)
            },
            rules: rules
        }), $(document).on("click", "#form_register div.button", function(e) {
            var form = $("#form_register"),
                m_method = form.attr("method"),
                m_action = form.attr("action"),
                m_data = form.serialize();
            return $("#form_register").valid() && $.ajax({
                type: m_method,
                url: m_action,
                data: m_data,
                resetForm: "true",
                success: function(result) {
                    var data = $(result).find("#registration_form"),
                        success = data.find(".success");
                    success.length > 0 && ($("#registration_form").find(".result").html("<div class='modal-template show' id='success'><div class='success_text_form'><a href='#' class='modal-template-close'>&times;</a><h3>Ваш запрос на регистрацию принят.</h3><p>Подтверждение регистрации будет направлено по электронной почте</p></div></div>"), $("div").remove(".error"), form.find('input[type="text"]').val(""), form.find('input[type="text"]').attr("value", ""), form.find("textarea").val(""), form.find("textarea").text(""))
                	setTimeout(function(){
                		$('.modal-template.show').remove();
                	},5000);
                }
            }), !1
        })
    }
    $("a.nav").click(function() {
        return $("html, body").animate({
            scrollTop: $("#" + $(this).data("href")).offset().top
        }, 2e3), !1
    });
    $(document).on('click', '.modal-template-close', function(e){
    	e.preventDefault();
    	$(this).parents('.modal-template').remove();
    });
});