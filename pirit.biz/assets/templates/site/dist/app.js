$(document).ready(function(){function show_disk_type(type,container){$("#disk_type button").removeClass("active"),$("#"+type).addClass("active"),$("#trisolini_area .disk_type_container select, #trisolini_area  .disk_type_container input, #disproving_area .disk_type_container select, #disproving_area .disk_type_container input").attr({disabled:"disabled"}),$("#"+container+"_area .disk_type_container").removeClass("hidden").removeClass("active").hide(),$("#"+container+"_"+type+" select, #"+container+"_"+type+" input").removeAttr("disabled"),$("#"+container+"_"+type).removeClass("hidden").addClass("active").show()}function price_all_calculate(name,price){var price_all=$("#price_all"),price_all_count=$("#price_all_count"),price_all_hidden=$(".price_all[name='price_all']"),price_all_count_hidden=$(".price_all[name='price_all_count']");null!==price&&$("#price_info").data(name,price),console.log($("#price_info").data());var items=0;$.each($("#price_info").data(),function(index,value){"disk_type"!==index&&(items=Number(items)+Number(value))}),price_all.text(items),price_all_hidden.val(items),price_all_count.text(items*$("#server_count").val()),price_all_count_hidden.val(items*$("#server_count").val())}if($("#menu_top div.parent").click(function(){var childs=$(this).find(".childs"),result=childs.hasClass("opened");result?(childs.slideUp(300),childs.removeClass("opened"),$("#menu_top .parent>span span").removeClass("togler")):($("#menu_top .childs").slideUp(300),$("#menu_top .childs").removeClass("opened"),$("#menu_top .parent>span  span").removeClass("togler"),childs.slideDown(300).addClass("opened"),childs.addClass("opened"),$(this).find(".togler_area").addClass("togler"))}),$("#menu_top div.main").mouseout(function(){$("body").hasClass("menu_mobile_open")||$(this).find(".childs_wrapper").removeClass("menu_hover")}),$("#nav_button").click(function(){$("#menu_top").toggleClass("open"),$("body").toggleClass("menu_mobile_open")}),$(document).on("click","body.menu_mobile_open #menu_top a.main",function(event){var parent=$(this).closest("div"),childs=parent.find(".childs_wrapper");return childs.length>0?(childs.toggleClass("menu_hover"),parent.find("a.main i").toggleClass("fa-minus"),event.stopPropagation(),!1):void 0}),$(window).resize(function(){$("#nav_button").is(":hidden")?($("body").removeClass("menu_mobile_open"),$("#menu_top .childs_wrapper").removeClass("menu_hover")):($("#menu_top").find("a.main i").removeClass("fa-minus"),$("body").removeClass("menu_mobile_open"),$("#menu_top").removeClass("open"))}),$(document).mouseup(function(e){var div=$("#menu_top div.parent");div.is(e.target)||0!==div.has(e.target).length||($("#menu_top .childs").slideUp(300),$("#menu_top .parent>span span").removeClass("togler"),$("#menu_top .childs").removeClass("opened"))}),$("#form_order").length>0){var rules={name:{required:!0},phone:{required:!0},email:{required:!0},comment:{required:!0}};$.validator.messages.required=null,$("#form_order").validate({wrapper:null,errorLabelContainer:null,errorContainer:null,focusInvalid:!1,focusCleanup:!0,submitHandler:function(form){},errorPlacement:function(){},invalidHandler:function(event,validator){validator.numberOfInvalids()&&$("html, body").animate({scrollTop:$(validator.errorList[0].element).offset().top},1e3)},rules:rules}),$(document).on("click","#form_order input.submit",function(e){var form=$("#form_order"),m_method=form.attr("method"),m_action=form.attr("action"),m_data=form.serialize();return $("#form_order").valid()&&$.ajax({type:m_method,url:m_action,data:m_data,resetForm:"true",success:function(result){var data=$(result).find("#feedback_order"),success=data.find(".success");success.length>0&&(window.location.href="https://pirit.biz/feedback-list/feedback-question-contacts")}}),!1})}if($(".bxslider").length>0){var slider=$(".bxslider").bxSlider();slider.startAuto()}if($("#calculator").length>0){$(".carousel").carousel(),$("#carousel-calulator").find(".item:first").addClass("active");var default_disk=$("#calculator").data("server-type");if(default_disk="30"===default_disk?"SATA":"SAS",$("#disk_type button").click(function(){var container=$("#disk_size button.active").attr("id");if($(this).hasClass("active"))var type=$(this).attr("id");else{$("#disk_type button").removeClass("active"),$(this).addClass("active");var type=$(this).attr("id")}$("#disk_type_value").val(type),show_disk_type(type,container)}),$("#disk_size button").click(function(){if($(this).hasClass("active"))var container=$(this).attr("id");else{$("#disk_size button").removeClass("active"),$(this).addClass("active");var container=$(this).attr("id")}$("#calculator .area").hide(),$("#"+container+"_area").removeClass("hidden").show(),show_disk_type(default_disk,container)}),$(document).on("change","#server_count",function(e){price_all_calculate(null,null)}),$(document).on("change",".items_info",function(e){var parent=$(this).parent(),parent_name=parent.closest(".row_item").data("name"),price=$(this).find("option:selected").data("price"),count=parent.find(".count option:selected").val();count>0?(price*=count,parent.find("span.price").text(price),parent.find("input.price").val(price),price_all_calculate(parent_name,price)):(parent.find(".count option").removeAttr("selected"),parent.find(".count option:first").attr("selected","selected"),parent.find(".count").val(parent.find(".count option:first").val()),parent.find("span.price").text("0"),parent.find("input.price").val("0"),price_all_calculate(parent_name,0))}),$(".items_info").each(function(e){var parent=$(this).parents(".row_item");parent.find("input.single_price").val($(this).find("option:first").data("price"))}),$(document).on("change",".count",function(e){var parent=$(this).parents(".row_item"),parent_name=parent.data("name"),price=parent.find(".items_info").find("option:selected").data("price"),all_price=price*$(this).val();parent.find("span.price").text(all_price),parent.find("input.price").val(all_price),parent.find("input.single_price").val(price),price_all_calculate(parent_name,all_price)}),$(document).on("click","#disproving",function(e){$("#price_info").data("disk_type","2,5")}),$(document).on("click","#trisolini",function(e){$("#price_info").data("disk_type","3,5")}),$(document).on("click","#disk_type button,#disk_size button",function(e){var parent=$(this).parents(".row_item");parent.find(".count option").removeAttr("selected"),parent.find(".count option:first").attr("selected","selected"),parent.find(".count").val(parent.find(".count option:first").val()),parent.find("span.price").text("0"),parent.find("input.price").val("0"),price_all_calculate(default_disk,0),price_all_calculate("SSD",0),price_all_calculate("SAS",0)}),$(document).on("change","#prosessor_count",function(e){var prosessor=$(this);if("double"===prosessor.data("memory")){var max=0;max="0"===prosessor.val()||"1"===prosessor.val()?$("#memory_count").data("max"):2*$("#memory_count").data("max"),$("#memory_count").empty();for(var i=$("#memory_count").data("min");max>=i;i++)$("#memory_count").append('<option value="'+i+'">'+i+"</option>");var parent=$("#memory");parent.find(".count option:first").attr("selected","selected"),parent.find(".count").val(parent.find(".count option:first").val()),parent.find(".count").trigger("click")}}),price_all_calculate("block_price",$("input[name='block_price']").val()),$("#calculator .count").trigger("change"),$("#form_server").length>0){var rules={name:{required:!0},phone:{required:!0},email:{required:!0},organization:{required:!0}};$.validator.messages.required=null,$("#form_server").validate({wrapper:null,errorLabelContainer:null,errorContainer:null,focusInvalid:!1,focusCleanup:!0,submitHandler:function(form){return $.ajax({type:"post",url:"/assets/templates/site/templates/chunks/calculator/response/index.php",data:$("#form_server").serialize(),resetForm:"true",success:function(response){var response=$.parseJSON(response);if("success"===response.status){grecaptcha.reset($("#g-recaptcha-calculator").data("captcha_id")),$(".captcha").removeClass("error_name");var order="<p><b>1. Шасси сервера (1 шт.)</b>: "+$("#block .block_name:last").text()+" - <b>"+$("#block .price").text()+"$</b></p>",items=["prosessor","controller","memory","disk","raid","ethernet","fc","power"],index=2;items.forEach(function(item,i,arr){var server_item=$("#calculator .row_item[data-name='"+item+"']");if(void 0!==server_item.data("name"))if("disk"==server_item.data("name")){var area="disproving";area=$("#disproving").hasClass("active")?"disproving_area":"trisolini_area",server_item=$("#"+area+" .disk_type_container.active");var count=Number(server_item.find(".count option:selected").text());count>0&&(order+="<p><b>"+index+". Жесткий диск ("+count+" шт.)</b>: "+server_item.find(".items_info option:selected").text()+" - <b>"+server_item.find(".price").text()+"$</b></p>",index++)}else{var count=Number(server_item.find(".count option:selected").text());count>0&&(order+="<p><b>"+index+". "+server_item.find("h3:first").text()+" ("+count+" шт.)</b>: "+server_item.find(".items_info option:selected").text()+" - <b>"+server_item.find(".price").text()+"$</b></p>",index++)}}),order+="<p>Бюджетная оценка стоимости оборудования составляет: <b>"+$("#price_all_count").text()+"$</b></p>",$("#server_order_content").html(order),$("#calculator_modal").modal("show")}else $(".captcha").addClass("error_name")}}),!1},errorPlacement:function(){},invalidHandler:function(event,validator){validator.numberOfInvalids()&&$("html, body").animate({scrollTop:$(validator.errorList[0].element).offset().top-150},1e3)},rules:rules})}}if($(".zadat_vopros").click(function(e){e.preventDefault(),$("#zadat_vopros").modal("show")}),$(".zakazat_zvonok").click(function(e){e.preventDefault(),$("#zakazat_zvonok").modal("show")}),$("#feedback_call").length>0){var rules={fio:{required:!0},phone:{required:!0}};$.validator.messages.required=null,$("#form_call").validate({wrapper:null,errorLabelContainer:null,errorContainer:null,focusInvalid:!1,focusCleanup:!0,submitHandler:function(form){},errorPlacement:function(){},invalidHandler:function(event,validator){!validator.numberOfInvalids()},rules:rules}),$(document).on("click","#form_call button.submitButton",function(e){var form=$("#form_call"),m_method=form.attr("method"),m_action=form.attr("action"),m_data=form.serialize();return $("#form_call").valid()&&$.ajax({type:m_method,url:m_action,data:m_data,resetForm:"true",success:function(result){var data=$(result).find("#feedback_call"),success=data.find(".success");success.length>0&&(window.location.href="https://pirit.biz/feedback-list/feedback-call"),data.find(".errors .alert").length>0&&$("#feedback_call .errors").html(data.find(".errors").html())}}),!1})}if($("#feedback_question").length>0){var rules={name:{required:!0},phone:{required:!0},email:{required:!0},comment:{required:!0}};$.validator.messages.required=null,$("#form_question").validate({wrapper:null,errorLabelContainer:null,errorContainer:null,focusInvalid:!1,focusCleanup:!0,submitHandler:function(form){},errorPlacement:function(){},invalidHandler:function(event,validator){!validator.numberOfInvalids()},rules:rules}),$(document).on("click","#form_question button.submitButton",function(e){var form=$("#form_question"),m_method=form.attr("method"),m_action=form.attr("action"),m_data=form.serialize();return $("#form_question").valid()&&$.ajax({type:m_method,url:m_action,data:m_data,resetForm:"true",success:function(result){var data=$(result).find("#feedback_question"),success=data.find(".success");success.length>0&&(window.location.href="https://pirit.biz/feedback-list/feedback-question"),data.find(".errors .alert").length>0&&$("#feedback_question .errors").html(data.find(".errors").html())}}),!1})}});

$ = jQuery;
$(document).click(function (event)
{
    if ($(event.target).closest(".b-navigation ul li ul").length)
        return;
    $('.b-navigation ul li ul').slideUp(300);
    $('.b-navigation li').removeClass('active');

    event.stopPropagation();
});
$(document).ready(function ()
{
    $('.b-navigation li .toggle-menu-items').click(function ()
    {
        if ($(this).parent('li').hasClass('active'))
        {
            $('.b-navigation li').removeClass('active');
            $(this).parent('li').removeClass('active');
            $('.b-navigation li ul').slideUp(300);
            $(this).siblings('ul').slideUp(300);
        }
        else
        {
            $('.b-navigation li').removeClass('active');
            $(this).parent('li').addClass('active');
            $('.b-navigation li ul').slideUp(300);
            $(this).siblings('ul').slideDown(300);
        }
        
        return false;
    });


    
    $('.nav-menu-mobile').click(function ()
    {
        $('.b-navigation').fadeToggle(300);
        if ($(this).hasClass('nav-close'))
        {
            $(this).removeClass('nav-close');
        }
        else
        {
            $(this).addClass('nav-close');
        }
        return false;
    });
    
    var user = detect.parse(navigator.userAgent);

    $name_browser = user.browser.family;
    $version_browser = user.browser.version;
    $os_name_browser = user.os.name;
    $bg = '#1d539e';

    // if ($name_browser == 'Firefox' || $name_browser == 'IE') {
    //     $('.b-frontVideo').css('background', $bg);
    //     $('.b-header').css('background', $bg);
    // }
    $('.b-frontVideo__slider-inner').owlCarousel({
	    loop:true,
	    nav:false,
	    dots:true,
		items:1
	});
	
});

$(window).on('scroll', function ()
{
    if (jQuery('body').width() < 992)
    {
    
    }
});

;(function ($, window, document, undefined)
{

})(jQuery, window, document);
